// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/HealthComponent.h"
#include "StateObject.h"
#include "CharacterHealthComponent.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

UCLASS()
class MYGAME_API UCharacterHealthComponent : public UHealthComponent
{
	GENERATED_BODY()
public:

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldChange OnShieldChange;

	FTimerHandle TimerHandle_CallDownShieldTimer;

	FTimerHandle TimerHandle_ShieldRecoverRateTimer;

	AActor* myActor = nullptr;

protected:
		virtual void BeginPlay() override;
		float Shield = 100.0f;

public:
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecover = 10.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Shield")
		float CoolDownShieldRecoverTime = 5.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoveryValue = 1.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoveryRate = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		UParticleSystem* ParticleEffectShield = nullptr;
	UParticleSystemComponent* ParticleEmitterShield = nullptr;

	void ChangeHealthValue(float ChangeValue) override;

	float GetCurrentShield();

	void ChangeShieldValue(float ChangeValue);

	void CoolDownShieldEnd();

	void RecoveryShield();
};

