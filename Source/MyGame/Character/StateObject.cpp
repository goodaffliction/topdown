// Fill out your copyright notice in the Description page of Project Settings.


#include "StateObject.h"
#include "CharacterHealthComponent.h"
#include "Interface/IGameActor.h"
#include "Kismet/GameplayStatics.h"

bool UStateObject::InitObject(AActor* Actor)
{
	myActor = Actor;

	IIGameActor* myInterface = Cast<IIGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}

	return true;
}

void UStateObject::DestroyObject()
{
	IIGameActor* myInterface = Cast<IIGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

//=======================================================

bool UStateObject_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();
	return true;
}

void UStateObject_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UStateObject_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UCharacterHealthComponent* myHealthComp = 
			Cast<UCharacterHealthComponent>(myActor->GetComponentByClass(UCharacterHealthComponent::StaticClass()));
		if (myHealthComp && myHealthComp->GetCurrentHealth() < 100.0f)
		{
			myHealthComp->ChangeHealthValue(Power);
			if (ParticleEffectOnce)
			{
				FName NameBoneToAttached;
				FVector EffectLocation;
				ParticleEmitterOnce = UGameplayStatics::SpawnEmitterAttached(ParticleEffectOnce, myActor->GetRootComponent(),
					NameBoneToAttached, EffectLocation, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}
		}
	}
	
	DestroyObject();
}

//==========================================================

bool UStateObject_ExecuteTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer,
		this, &UStateObject_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer,
		this, 	&UStateObject_ExecuteTimer::Execute, RateTime, true);

	if (ParticleEffect) 
	{
		FName NameBoneToAttached;
		FVector EffectLocation;
		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(),
			NameBoneToAttached, EffectLocation, FRotator::ZeroRotator, EffectOffset, EAttachLocation::SnapToTarget,false);
	}
	return true;
}

void UStateObject_ExecuteTimer::DestroyObject()
{
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void UStateObject_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UHealthComponent* MyHealthComp = 
			Cast<UHealthComponent>(myActor->GetComponentByClass(UHealthComponent::StaticClass()));
		if (MyHealthComp)
		{
			MyHealthComp->ChangeHealthValue(Power);
		}
	}
}
//=======================================================
bool UStateObject_ExecuteOnceShield::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();
	return true;
}

void UStateObject_ExecuteOnceShield::DestroyObject()
{
	Super::DestroyObject();
}



void UStateObject_ExecuteOnceShield::ExecuteOnce()
{
	if (myActor)
	{
		UCharacterHealthComponent* myHealthComp =
			Cast<UCharacterHealthComponent>(myActor->GetComponentByClass(UCharacterHealthComponent::StaticClass()));
		if (myHealthComp && myHealthComp->GetCurrentShield() > 0.0f)
		{
			myHealthComp->ChangeShieldValue(Power);
			if (ParticleEffectShield)
			{
				FName NameBoneToAttached;
				FVector EffectLocation;
				ParticleEmitterShield = UGameplayStatics::SpawnEmitterAttached(ParticleEffectShield, myActor->GetRootComponent(),
					NameBoneToAttached, EffectLocation, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}
		}
	}
	DestroyObject();
}
