// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "StateObject.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class MYGAME_API UStateObject : public UObject
{
	GENERATED_BODY()
public:

	virtual bool InitObject(AActor* Actor);
	virtual void DestroyObject();

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Settings");
	TArray<TEnumAsByte<EPhysicalSurface>> PosibleInteractSurface;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Settings");
	bool bIsStakable = false;

	AActor* myActor = nullptr;
};

UCLASS(Blueprintable)
class MYGAME_API UStateObject_ExecuteOnce : public UStateObject
{
	GENERATED_BODY(Blueprintable)

public:
	
	bool InitObject(AActor* Actor) override;
	void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
		float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
		UParticleSystem* ParticleEffectOnce = nullptr;
	UParticleSystemComponent* ParticleEmitterOnce = nullptr;
};

UCLASS(Blueprintable)
class MYGAME_API UStateObject_ExecuteTimer : public UStateObject
{
	GENERATED_BODY()

public:

	bool InitObject(AActor* Actor) override;
	void DestroyObject() override;
	
	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Period")
		float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Period")
		float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Period")
		float RateTime = 1.0f;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Period")
		UParticleSystem* ParticleEffect = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Period ")
		FVector EffectOffset = (FVector(1.0f));

	UParticleSystemComponent* ParticleEmitter = nullptr;
};

UCLASS(Blueprintable)
class MYGAME_API UStateObject_ExecuteOnceShield : public UStateObject
{
	GENERATED_BODY(Blueprintable)

public:

	bool InitObject(AActor* Actor) override;
	void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
		float Power = 0.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
		UParticleSystem* ParticleEffectShield = nullptr;
	UParticleSystemComponent* ParticleEmitterShield = nullptr;
};