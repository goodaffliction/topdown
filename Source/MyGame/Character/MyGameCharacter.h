// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "WeaponDefault.h"
#include "GameFramework/Character.h"
#include "FunctionLibrary/Types.h"
#include "Game/InventoryComponent.h"
#include "CharacterHealthComponent.h"
#include "Interface/IGameActor.h"
#include "StateObject.h"
#include "MyGameCharacter.generated.h"


UCLASS(Blueprintable)
class AMyGameCharacter : public ACharacter, public IIGameActor
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;

public:
	AMyGameCharacter();
	FTimerHandle TimerHandle_RagDollTimer;
	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCharacterHealthComponent* HealthComponent;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	public:
		//Cursor
		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Cursor")
			UMaterialInterface* CursorMaterial = nullptr;
		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Cursor")
			FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

		UDecalComponent* CurrentCursor = nullptr;

		//Movement
		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
			EMovementState MovementState = EMovementState::Run_State;

		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
			FCharacterSpeed MovementSpeedInfo;

		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
			bool SprintRunEnable = false;
		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
			bool WalkEnable = false;
		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
			bool AimEnable = false;
		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ReloadLogic")
			bool bIsReloading = false;
		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "HealthLogic")
			bool bIsAlive = true;

		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "HealthLogic")
			TArray<UAnimMontage*> DeathAnim;
		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Ability")
			TSubclassOf<UStateObject> AbilityEffect;
		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Ability")
			TSubclassOf<UStateObject> ShieldDamaged;
		//Weapon	
		AWeaponDefault* CurrentWeapon = nullptr;
		
		//Effect
		TArray<UStateObject*> Effects;

		//Inputs
		UFUNCTION()
			void InputAxisX(float value);
		UFUNCTION()
			void InputAxisY(float value);
		UFUNCTION()
			void InputAttackPressed();
		UFUNCTION()
			void InputAttackReleased();

			float AxisX = 0.0f;
			float AxisY = 0.0f;

		//Tick function
		UFUNCTION()
		void MovementTick(float DeltaTime);

		//Functions=========================
		UFUNCTION(BlueprintCallable)
			UDecalComponent* GetCursorToWorld();

		//State Functions
		UFUNCTION(BlueprintCallable)
			void AttackCharEvent(bool bIsFiring);
		UFUNCTION(BlueprintCallable)
			void CharacterUpdate();
		UFUNCTION(BlueprintCallable)
			void ChangeMovementState();

		//Weapon, Reload
		UFUNCTION(BlueprintCallable)
			AWeaponDefault* GetCurrentWeapon();
		UFUNCTION(BlueprintCallable)
			void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
		UFUNCTION(BlueprintCallable)//VisualOnly
			void RemoveCurrentWeapon();
		UFUNCTION(BlueprintCallable)
			void TryReloadWeapon();

		//Animation Functions
		UFUNCTION()
			void WeaponReloadStart(UAnimMontage* Anim);
		UFUNCTION()
			void WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake);

		UFUNCTION(BlueprintNativeEvent)
			void WeaponReloadStart_BP(UAnimMontage* Anim);
		UFUNCTION(BlueprintNativeEvent)
			void WeaponReloadEnd_BP(bool bIsSuccess);

		UFUNCTION()
			void WeaponFireStart(UAnimMontage* Anim);
		UFUNCTION(BlueprintNativeEvent)
			void WeaponFireStart_BP(UAnimMontage* Anim);

		//Inventory Func
		void TrySwicthNextWeapon();
		void TrySwitchPreviosWeapon();
		UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
			int32 CurrentIndexWeapon = 0;

		//Ability
		void TryAbilityEnabled();
		void DebugShieldDamaged();

		//
		UFUNCTION()
		void CharDead();
		void EnableRagdoll();
		virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

		//Interface
		EPhysicalSurface GetSurfaceType() override;
		TArray<UStateObject*> GetAllCurrentEffects() override;
		void RemoveEffect(UStateObject* RemoveEffect) override;
		void AddEffect(UStateObject* newEffect) override;
};