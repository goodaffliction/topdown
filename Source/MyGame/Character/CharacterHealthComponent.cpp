// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterHealthComponent.h"
#include "Kismet/GameplayStatics.h"

void UCharacterHealthComponent::BeginPlay()
{
	Super::BeginPlay();
	if (ParticleEffectShield)
	{
		if (!ParticleEmitterShield)
		{
			FName NameBoneToAttached;
			FVector EffectLocation;
			ParticleEmitterShield = UGameplayStatics::SpawnEmitterAttached(ParticleEffectShield, GetOwner()->GetRootComponent(),
				NameBoneToAttached, EffectLocation, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
	}
}

void UCharacterHealthComponent::ChangeHealthValue(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		if (Shield < 0.0f)
		{		}
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}
}

float UCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;
	if (Shield > 0.0 && ParticleEffectShield)
	{
		if (Shield > 100.0f)
		{
			Shield = 100.0f;
		}
	}
	else
	{
		if (Shield <= 0.0f && ParticleEmitterShield)
		{
			Shield = 0.0f;
			ParticleEmitterShield->DestroyComponent();
			ParticleEmitterShield = nullptr;
		}
	}
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CallDownShieldTimer, 
			this, &UCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoverRateTimer);
	}
	OnShieldChange.Broadcast(Shield, ChangeValue);
}

void UCharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoverRateTimer, 
			this, &UCharacterHealthComponent::RecoveryShield, ShieldRecoveryRate, true);
	}
}

void UCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoveryValue;
	if (ParticleEffectShield)
	{
		if (!ParticleEmitterShield)
		{
			FName NameBoneToAttached;
			FVector EffectLocation;
			ParticleEmitterShield = UGameplayStatics::SpawnEmitterAttached(ParticleEffectShield, GetOwner()->GetRootComponent(),
				NameBoneToAttached, EffectLocation, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
	}
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoverRateTimer);
		}
	}
	else
	{
		Shield = 100.0f;
	}
	OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
}
