// Fill out your copyright notice in the Description page of Project Settings.


#include "EnviromentStructure.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
AEnviromentStructure::AEnviromentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEnviromentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnviromentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface AEnviromentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		if (myMesh->GetMaterial(0))
		{
			UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
			if (myMaterial)
			{
				Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
			}
		}
	}
	return Result;
}

TArray<UStateObject*> AEnviromentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void AEnviromentStructure::RemoveEffect(UStateObject* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void AEnviromentStructure::AddEffect(UStateObject* newEffect)
{
	Effects.Add(newEffect);
}

