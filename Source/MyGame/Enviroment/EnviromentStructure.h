// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/IGameActor.h"
#include "Character/StateObject.h"
#include "EnviromentStructure.generated.h"

UCLASS()
class MYGAME_API AEnviromentStructure : public AActor, public IIGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnviromentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfaceType() override;

	UPROPERTY(EditAnywhere, BlueprintreadWrite, Category = "Settings")
	TArray<UStateObject*> Effects;

	TArray<UStateObject*> GetAllCurrentEffects() override;
	void RemoveEffect(UStateObject* RemoveEffect) override;
	void AddEffect(UStateObject* newEffect) override;
};
